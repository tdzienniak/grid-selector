(function ($) {
  $.fn.gridSelector = function (options) {
    if (this.length !== 1 && !this.is('div')) {
      throw new Error('Zła liczba lub typ elementow.');
    }

    if (this.data('gridSelector') != null) {
      return this.data('gridSelector');
    }

    return init(this, options);
  };

  function init(element, options) {
    var gridSelectorInstance = GridSelectorFactory(element, options);

    element.data('gridSelector', gridSelectorInstance);

    return gridSelectorInstance;
  }

  var GridSelectorFactory = function (element, options) {
    var instance = {};
    var table = element.find('table.gs-selectable');
    var settings = $.extend({
      distance: 10,
      events: {},
      format: '_DDMMYYYY'
    }, options);

    var events = settings.events;
    var cellsByRow = {};

    var eventBeingOperatedOn;

    // select variables
    var isSelectionInProgress = false;
    var rowBeingSelected;
    var selectionStartingCell;
    var startOffset;
    var currentOffset;

    // move variables
    var isMovingInProgress = false;
    var eventMovingStartOffset;
    var maxLeftMove;
    var maxRightMove;
    var eventHasMoved = false;

    // resize variables
    var isResizingInProgress = false;
    var resizeDirection;
    var maxLeftResize;
    var maxRightResize;
    var eventResizingStartOffset;
    var eventHasResized = false;

    table.find('tr[data-id]').each(function () {
      var row = $(this);
      var rowId = row.data('id');

      var cellsInRow = [];

      row.find('td[data-id]').each(function () {
        cellsInRow.push($(this).data('id'));
      })

      cellsByRow[rowId] = cellsInRow;
    })

    initEvents(events);

    element.on('click', '.gs-event', function (e) {
      if ($(this).data('cancelClick')) {
          $(this).removeData('cancelClick');

          return;
      }

      var target = $(e.target);
      var eventDiv = $(this);
      var event = getEventById(eventDiv.data('id'));

      if (target.is('.gs-remove-icon')) {
        var shouldRemove = true;

        if (typeof settings.onRemoveButtonClick === 'function') {
          shouldRemove = settings.onRemoveButtonClick({
            instance: instance,
            elem: eventDiv,
            event: event
          }) !== false;
        }

        if (shouldRemove) {
          removeEventById(event.id);
        }
      } else {
        if (typeof settings.onEventClick === 'function') {
          settings.onEventClick({
            event: event,
            elem: eventDiv
          })
        }
      }
    })

    table.find('td[data-id]').on('mousedown', cellMouseDown);

    $(document).mouseup(function (e) {
      var clickWontBeTriggered = false;

      if (eventBeingOperatedOn && $(e.target).closest('.gs-event').is(eventBeingOperatedOn.element) && (eventHasMoved || eventHasResized)) {
        eventBeingOperatedOn.element.data('cancelClick', true);
        clickWontBeTriggered = true;
      }

      if (isSelectionInProgress) {
        $(document).off('mousemove', recordSelection);

        var selectedCells = table.find('tr[data-id=' + rowBeingSelected + '] td[data-id].selected');

        if (selectedCells.length) {
          var fromCellId = selectedCells.first().data('id');
          var toCellId = selectedCells.last().data('id');
          var shouldCancel = false;

          var newEvent = initEvent(rowBeingSelected, {
            fromCellId: fromCellId,
            toCellId: toCellId,
            id: guid()
          });

          if (typeof settings.onNewEvent === 'function') {
            shouldCancel = settings.onNewEvent({
              event: newEvent,
            }) === false;
          }

          if (!shouldCancel) {

            events[rowBeingSelected] = events[rowBeingSelected] || [];
            events[rowBeingSelected].push(newEvent);

            update();
          }
        }

        //cleanup
        isSelectionInProgress = false;
        rowBeingSelected = null;
        startOffset = null;
        currentOffset = null;

        selectedCells.removeClass('selected');
      }

      if (isMovingInProgress || isResizingInProgress) {
        var shouldChange = true;

        if (isMovingInProgress) {
          $(document).off('mousemove', recordEventMoving);

          if (typeof settings.onEventMoved === 'function' && clickWontBeTriggered) {
            shouldChange = settings.onEventMoved({
              event: eventBeingOperatedOn
            })
          }

          //move cleanup
          isMovingInProgress = false;
          maxLeftMove = null;
          maxRightMove = null;
          eventHasMoved = false;
        } else if (isResizingInProgress) {
          $(document).off('mousemove', recordEventResizing);

          if (typeof settings.onEventMoved === 'function' && clickWontBeTriggered) {
            shouldChange = settings.onEventResized({
              event: eventBeingOperatedOn
            })
          }

          //resize cleanup
          isResizingInProgress = false;
          eventHasResized = false;
        }

        //cancel move or resize
        if (shouldChange === false) {
          eventBeingOperatedOn.fromCellId = eventBeingOperatedOn.originalFromCellId;
          eventBeingOperatedOn.toCellId = eventBeingOperatedOn.originalToCellId;
          eventBeingOperatedOn.fromDate = eventBeingOperatedOn.originalFromDate;
          eventBeingOperatedOn.toDate = eventBeingOperatedOn.originalToDate;
          eventBeingOperatedOn.cells = eventBeingOperatedOn.originalCells;

          update();
        }

        //cleanup
        delete eventBeingOperatedOn.originalFromCellId;
        delete eventBeingOperatedOn.originalToCellId;
        delete eventBeingOperatedOn.originalFromDate;
        delete eventBeingOperatedOn.originalToDate;
        delete eventBeingOperatedOn.originalCells;

        eventBeingOperatedOn = null;
      }

      $('body').css('cursor', '');
    });

    element.on('mousedown', '.gs-event', eventMouseDown);

    // --------
    // HANDLERY
    // --------

    function eventMouseDown(e) {
      var target = $(e.target);
      var eventElement = $(this);
      var eventId = eventElement.data('id');
      var event = instance.getEventById(eventId);

      eventBeingOperatedOn = event;

      event.originalFromCellId = event.fromCellId;
      event.originalToCellId = event.toCellId;
      event.originalFromDate = event.fromDate;
      event.originalToDate = event.toDate;
      event.originalCells = event.cells;

      if (target.is('.gs-event-left-edge')) {
        edgeMouseDown(e, event, 'left');
      } else if (target.is('.gs-event-right-edge')) {
        edgeMouseDown(e, event, 'right');
      } else {
        isMovingInProgress = true;
        eventMovingStartOffset = e.pageX;

        /**
         * Obliczanie o ile mozna przesunąć dane wydarzenie w prawo i w lewo.
         * Uwzględniane są krańce widocznego grida jak i wydarzenia okalające.
         */
        maxRightMove = countMaxRightMove(event);
        maxLeftMove = countMaxLeftMove(event);

        $('body').css('cursor', 'move');
        $(document).on('mousemove', recordEventMoving);
      }
    }


    function cellMouseDown(e) {
      var cell = $(this);
      var cellId = cell.data('id');
      var rowId = cell.closest('tr[data-id]').data('id');

      if (!isCellFree(rowId, cellId)) {
        return;
      }

      isSelectionInProgress = true;
      selectionStartingCell = cell;
      rowBeingSelected = rowId;
      startOffset = e.pageX;

      $('body').css('cursor', 'ew-resize');
      $(document).on('mousemove', recordSelection);
    }

    function edgeMouseDown(originalEvent, event, direction) {
      isResizingInProgress = true;
      resizeDirection = direction;
      eventResizingStartOffset = originalEvent.pageX;

      var firstCellInRow = cellsByRow[event.rowId][0];
      var lastCellInRow = cellsByRow[event.rowId][cellsByRow[event.rowId].length - 1];



      if (resizeDirection === 'left') {
        maxLeftResize = countLeftGap(eventBeingOperatedOn);
        maxRightResize = Math.min(event.cells.length - 1, distanceBetweenCells(event.fromCellId, lastCellInRow));
      } else {
        maxLeftResize = Math.min(-(event.cells.length - 1), distanceBetweenCells(event.toCellId, firstCellInRow));
        maxRightResize = countRightGap(eventBeingOperatedOn);
      }

      $(document).on('mousemove', recordEventResizing);
    }

    function recordEventResizing(e) {
      var delta = e.pageX - eventResizingStartOffset;
      var currentCell = getCellsBetweenOffsets(eventBeingOperatedOn.rowId, e.pageX, e.pageX, 0);
      var cells = getCellsBetweenOffsets(eventBeingOperatedOn.rowId, e.pageX, eventResizingStartOffset, 0)
        .filter(function () {
          return !$(this).is(currentCell);
        });


      var deltaSign = Math.sign(delta);
      var cellMovement = deltaSign * cells.length;

      if (maxLeftResize <= cellMovement && cellMovement <= maxRightResize) {
        if (cells.length > 0 && !eventHasResized) {
          eventHasResized = true;
        }

        resizeEvent(eventBeingOperatedOn, resizeDirection, cellMovement);

        // po przesunięciu zmienily sie datay krancowe, wiec trzeba ponownie znaleźć komorki wydarzenia
        eventBeingOperatedOn.cells = getEventCells(eventBeingOperatedOn);

        update();
      }


    }

    function recordEventMoving(e) {
      var delta = e.pageX - eventMovingStartOffset;
      var currentCell = getCellsBetweenOffsets(eventBeingOperatedOn.rowId, e.pageX, e.pageX, 0);
      var cells = getCellsBetweenOffsets(eventBeingOperatedOn.rowId, e.pageX, eventMovingStartOffset, 0)
        .filter(function () {
          return !$(this).is(currentCell);
        });

      var deltaSign = Math.sign(delta);
      var cellMovement = deltaSign * cells.length;

      if (maxLeftMove <= cellMovement && cellMovement <= maxRightMove) {
        if (cells.length > 0 && !eventHasMoved) {
          eventHasMoved = true;
        }

        moveEvent(eventBeingOperatedOn, cellMovement);

        // po przesunięciu zmienily sie datay krancowe, wiec trzeba ponownie znaleźć komorki wydarzenia
        eventBeingOperatedOn.cells = getEventCells(eventBeingOperatedOn);

        update();
      }
    }



    // function getCellO

    function getCellsBetweenOffsets(rowId, offset1, offset2, distance) {
      var minOffset = Math.min(offset1, offset2);
      var maxOffset = Math.max(offset1, offset2);

      var allRowCells = getRowCells(rowId);

      if (maxOffset - minOffset < distance) {
        return $();
      }

      var cellsBetweenOffsets = allRowCells.filter(function () {
        var cell = $(this);
        var cellOffset = cell.offset();

        var minX = cellOffset.left;
        var maxX = minX + cell.outerWidth()

        return maxX > minOffset && minX < maxOffset || minX > minOffset && maxX < maxOffset;
      })

      return cellsBetweenOffsets;
    }

    function getSelectedCellsBetweenOffsets(rowId, offset1, offset2, distance) {
      var selectedRowCells = getCellsBetweenOffsets(rowId, offset1, offset2, distance);
      var startingCellIndex = selectedRowCells.index(selectionStartingCell);

      if (startingCellIndex === 0) {
        var eventsStartingCells = getEventsStartingCells(rowId);

        for (var i = 1; i < selectedRowCells.length; i++) {
          var id = selectedRowCells.eq(i).data('id');

          if (eventsStartingCells.indexOf(id) !== -1) {
            selectedRowCells = selectedRowCells.slice(0, i);

            break;
          }
        }
      } else if (startingCellIndex > 0) {
        var eventsEndingCells = getEventsEndingCells(rowId);

        for (var i = startingCellIndex; i >= 0; i--) {
          var id = selectedRowCells.eq(i).data('id');

          if (eventsEndingCells.indexOf(id) !== -1) {
            selectedRowCells = selectedRowCells.slice(i + 1);

            break;
          }
        }
      }

      return selectedRowCells;
    }

    function recordSelection(e) {
      var currentOffset = e.pageX;

      getRowCells(rowBeingSelected).removeClass('selected');

      var selectedRowCells = getSelectedCellsBetweenOffsets(rowBeingSelected, startOffset, currentOffset, settings.distance);

      selectedRowCells.addClass('selected');
    }

    //INICJALIZACJA

    function initEvents(eventsByRows) {
      Object.keys(eventsByRows).forEach(function (rowId) {
        var eventsInRow = events[rowId];

        eventsInRow.forEach(function (event) {
          initEvent(rowId, event);
        });
      })
    }

    function initEvent(rowId, event) {
      if (!('id' in event)) {
        event.id = guid();
      }

      event.rowId = rowId;
      event.fromDate = parseDate(event.fromCellId, settings.format);
      event.toDate = parseDate(event.toCellId, settings.format);
      event.cells = getEventCells(event);

      return event;
    }

    function getEventCells(event) {
      var itr = moment.twix(event.fromDate, event.toDate).iterate("days");
      var cells = [];

      while (itr.hasNext()) {
        cells.push(dateToString(itr.next(), settings.format));
      }

      return cells;
    }

    function update() {
      renderEvents();
    }

    function renderEvents() {
      Object.keys(events).forEach(function (rowId) {
        var eventsInRow = events[rowId];
        var firstCellInRow = cellsByRow[rowId][0];
        var lastCellInRow = cellsByRow[rowId][cellsByRow[rowId].length - 1];

        eventsInRow.filter(function (event) {
          var eventEndingCellDistanceFromRowStart = distanceBetweenCells(event.toCellId, firstCellInRow);
          var eventStartingCellDistanceFromRowEnd = distanceBetweenCells(event.fromCellId, lastCellInRow);

          return !(eventEndingCellDistanceFromRowStart > 0 || eventStartingCellDistanceFromRowEnd < 0);
        }).forEach(function (event) {
          var eventDiv;

          if (event.element) {
            eventDiv = event.element;
          } else {
            eventDiv = createEventDiv(event);

            element.append(eventDiv);

            event.element = eventDiv;
          }

          var additionalClasses = [];

          var startingCell = getCellInRow(rowId, event.fromCellId);
          var endingCell = getCellInRow(rowId, event.toCellId);

          if (!startingCell.length) {
            startingCell = getRowCells(rowId).first();

            additionalClasses.push('gs-event-hidden-start')
          }

          if (!endingCell.length) {
            endingCell = getRowCells(rowId).last();

            additionalClasses.push('gs-event-hidden-end')
          }

          var startingCellPos = startingCell.position();
          var endingCellPos = endingCell.position();

          var cellHeight = startingCell.outerHeight();
          var cellWidth = startingCell.outerWidth();

          eventDiv.removeClass('gs-event-hidden-start').removeClass('gs-event-hidden-end');
          eventDiv.addClass(additionalClasses.join(' '));

          eventDiv.css({
            top: startingCellPos.top,
            left: startingCellPos.left
          });

          var paddingAndBorderVert = eventDiv.outerHeight(true) - eventDiv.height();
          var paddingAndBorderHor = eventDiv.outerWidth(true) - eventDiv.width();

          eventDiv.height(cellHeight - paddingAndBorderVert);
          eventDiv.width(endingCellPos.left - startingCellPos.left + cellWidth - paddingAndBorderHor);
        });
      })
    }

    /**
     * FUNKCJE POMOCNICZE
     */

    function moveEvent(event, count) {
      var newFromDate = event.originalFromDate.clone().add(count, 'days');
      var newToDate = event.originalToDate.clone().add(count, 'days');

      event.fromCellId = dateToString(newFromDate, settings.format);
      event.toCellId = dateToString(newToDate, settings.format);
      event.fromDate = newFromDate;
      event.toDate = newToDate;
    }

    function resizeEvent(event, direction, count) {
      if (direction === 'left') {
        var newFromDate = event.originalFromDate.clone().add(count, 'days');
        var newFromCellId = dateToString(newFromDate, settings.format);

        if (distanceBetweenCells(newFromCellId, event.toCellId) < 0) {
          return;
        }

        event.fromCellId = newFromCellId;
        event.fromDate = newFromDate;
      } else {
        var newToDate = event.originalToDate.clone().add(count, 'days');
        var newToCellId = dateToString(newToDate, settings.format);

        if (distanceBetweenCells(event.fromCellId, newToCellId) < 0) {
          return;
        }

        event.toCellId = newToCellId;
        event.toDate = newToDate;
      }
    }

    function countMaxLeftMove(event) {
      var eventEndingCellDistanceFromRowStart = distanceBetweenCells(event.toCellId, getRowCells(event.rowId).first().data('id'));
      var leftDistances = countLeftDistances(event);

      return Math.max.apply(null, [eventEndingCellDistanceFromRowStart].concat(leftDistances));
    }

    function countMaxRightMove(event) {
      var eventStartingCellDistanceFromRowEnd = distanceBetweenCells(event.fromCellId, getRowCells(event.rowId).last().data('id'));
      var rightDistances = countRightDistances(event);

      return Math.min.apply(null, [eventStartingCellDistanceFromRowEnd].concat(rightDistances));
    }

    function countLeftGap(event) {
      var leftDistances = countLeftDistances(event, true);

      return Math.max.apply(null, leftDistances);
    }

    function countRightGap(event) {
      var rightDistances = countRightDistances(event, true);

      return Math.min.apply(null, rightDistances);
    }

    function countLeftDistances(event, withRowEdge) {
      var cells = [];

      if (withRowEdge) {
        var preRowStartCell = dateToString(parseDate(getRowCells(event.rowId).first().data('id'), settings.format).subtract(1, 'days'), settings.format);

        cells.push(preRowStartCell);
      }

      return cells.concat(getEventsEndingCells(event.rowId)).map(function (cellId) {
        return distanceBetweenCells(event.fromCellId, cellId);
      }).filter(function (distance) {
        return distance < 0;
      }).map(function (distance) {
        return Math.sign(distance) * (Math.abs(distance) - 1);
      });
    }

    function countRightDistances(event, withRowEdge) {
      var cells = [];

      if (withRowEdge) {
        var postRowEndCell = dateToString(parseDate(getRowCells(event.rowId).last().data('id'), settings.format).add(1, 'days'), settings.format);

        cells.push(postRowEndCell);
      }

      return cells.concat(getEventsStartingCells(event.rowId)).map(function (cellId) {
        return distanceBetweenCells(event.toCellId, cellId);
      }).filter(function (distance) {
        return distance > 0;
      }).map(function (distance) {
        return Math.sign(distance) * (Math.abs(distance) - 1);
      });
    }

    function isCellFree(rowId, cellId) {
      if (Array.isArray(events[rowId])) {
        return events[rowId].every(function (event) {
          return event.cells.indexOf(cellId) === -1;
        });
      } else {
        return true;
      }
    }

    function createEventDiv(event) {
      var div = $('<div class="gs-event"><div class="gs-event-left-edge"></div><span class="gs-event-text"></span><div class="gs-event-right-edge"></div></div>');

      if (event.class) {
        div.addClass(event.class);
      }

      if (event.name) {
        div.find('.gs-event-text').text(event.name);
      }

      if (settings.showRemoveButton) {
        var removeButton = $('<img class="gs-remove-icon" src="' + settings.removeIconURL + '" />');

        div.append(removeButton);
      }

      div.attr('data-id', event.id);

      return div;
    }

    function getEventById(eventId) {
      var foundEvent;

      Object.keys(events).some(function (rowId) {
        return events[rowId].some(function (event) {
          if (event.id === eventId) {
            foundEvent = event;

            return true;
          }

          return false;
        })
      })

      return foundEvent;
    }

    function getRowCells(rowId) {
      return table.find('tr[data-id=' + rowId + '] td[data-id]');
    }

    function getCellInRow(rowId, cellId) {
      return table.find('tr[data-id=' + rowId + '] td[data-id=' + cellId + ']');
    }

    function getEventsStartingCells(rowId) {
      if (Array.isArray(events[rowId])) {
        return events[rowId].map(function (event) {
          return event.fromCellId;
        })
      } else {
        return [];
      }
    }

    function getEventsEndingCells(rowId) {
      if (Array.isArray(events[rowId])) {
        return events[rowId].map(function (event) {
          return event.toCellId;
        })
      } else {
        return [];
      }
    }

    function removeEventById(eventId) {
      var event = getEventById(eventId);

      if (event != null) {
        var eventIndex = events[event.rowId].indexOf(event);

        events[event.rowId].splice(eventIndex, 1);

        update();
      }
    }

    function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
    }

    function parseDate(idStr, format) {
      return moment.utc(idStr, format);
    }

    function dateToString(date, format) {
      return date.format(format);
    }

    function distanceBetweenCells(cellId1, cellId2) {
      return -parseDate(cellId1, settings.format).diff(parseDate(cellId2, settings.format)) / (1000 * 60 * 60 * 24)
    }

    //initialUpdate
    update();

    //exposing public API
    instance.getEventById = getEventById;
    instance.removeEventById = removeEventById;

    return instance;
  }
})(jQuery);
